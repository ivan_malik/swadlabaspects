package com.malik.Calculation;

public class Calc implements SimpleItn {
    public Double plus(double a, double b) {
        return a+ b;
    }

    public Long factorial(int n) {
        Long result;
        long r = 1;
        for(int i = 2; i <= n; i++){
            r *= i;
        }
        result = r;
        return result;
    }
}
