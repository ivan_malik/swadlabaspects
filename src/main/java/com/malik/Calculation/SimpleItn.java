package com.malik.Calculation;

public interface SimpleItn {

    public Double plus(double a, double b);

    public Long factorial(int n);
}
