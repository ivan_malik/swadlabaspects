package com.malik;

import com.malik.Calculation.SimpleItn;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class App {
    public static void main(String[] args) {
        ApplicationContext appContext = new ClassPathXmlApplicationContext(
                "config/spring-configuration.xml");

        SimpleItn simple = (SimpleItn) appContext.getBean("calc");
        simple.factorial(25);
        simple.factorial(3);
        simple.factorial(9);
        simple.plus(389000098,65678123);
    }
}
