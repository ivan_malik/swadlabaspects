package com.malik.dao;

import com.malik.entities.MethodInf;

public interface MethodInfDao {
    public void saveMethodInf(MethodInf methodInf);
}