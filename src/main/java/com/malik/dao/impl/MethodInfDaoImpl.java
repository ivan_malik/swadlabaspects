package com.malik.dao.impl;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.orm.hibernate4.HibernateTemplate;

import com.malik.dao.MethodInfDao;
import com.malik.entities.MethodInf;


public class MethodInfDaoImpl implements MethodInfDao {

    private HibernateTemplate hibernateTemplate;

    @Bean
    @Autowired
    public void setSessionFactory(SessionFactory sessionFactory) {
        HibernateTemplate hb = new HibernateTemplate();
        hb.setCheckWriteOperations(false);
        hb.setSessionFactory(sessionFactory);
        this.hibernateTemplate = hb;
    }

    @Override
    public void saveMethodInf(MethodInf methodInf) {
        hibernateTemplate.saveOrUpdate(methodInf);
    }

}