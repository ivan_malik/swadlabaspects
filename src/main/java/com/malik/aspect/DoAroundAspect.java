package com.malik.aspect;


import com.malik.dao.MethodInfDao;
import com.malik.entities.MethodInf;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.Arrays;

@Aspect
public class DoAroundAspect {
    @Around("execution(* com.malik.Calculation.Calc.*(..))")
    public void doAroundF(ProceedingJoinPoint joinPoint) throws Throwable {
        long startTime = System.currentTimeMillis();
        System.out.println(startTime);
        joinPoint.proceed();

        String methodName = joinPoint.getSignature().getName();
        String args = Arrays.toString(joinPoint.getArgs());

        long endTime = System.currentTimeMillis();
        System.out.println(endTime);
        long executionTime = (endTime - startTime);
        System.out.println(executionTime);

        ApplicationContext appContext = new ClassPathXmlApplicationContext(
                "config/spring-configuration.xml");

        MethodInf m1 = new MethodInf();
        m1.setName(methodName);
        m1.setTime(executionTime);
        m1.setArgs(args);

        MethodInfDao methodInfDao = (MethodInfDao) appContext.getBean("methodInfDao");
        methodInfDao.saveMethodInf(m1);
    }
}
